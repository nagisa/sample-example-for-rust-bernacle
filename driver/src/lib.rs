extern "C" {
    fn private_extern_symbol() -> u64;
}

#[inline(always)]
pub fn public_extern_wrapper() -> u64 {
    unsafe {
        private_extern_symbol()
    }
}
